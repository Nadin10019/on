#include "../inc/ush.h"


char *strchr(const char *s, int c){
	char	*str;

	str = (char *)s;
	while (*str != c){
		if (*str == '\0'){
			return (NULL);
		}
		str++;
	}
	return (str);
}

char *mystrtok(char * str, const char * delim, char **last) {
    if (str) {
        *last = str;
        }
    if ((*last == 0) || (**last == 0)) {
        return 0;
    }
    
    char *c = *last;
   
    while(strchr(delim, *c))
         ++c;
        if (*c == 0) 
            return 0;

    char *start = c;
    
    while(*c && (strchr(delim, *c)==0)) 
        ++c;
        if (*c == 0)
        {
            *last = c;
            return start;
        }
    *c = 0;
    *last = c+1;
    return start;
}

