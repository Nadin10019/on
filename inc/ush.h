#ifndef USH_H
#define USH_H

#include <libmx.h>
#include <stdlib.h>
//#include <string.h>
#define USH_TOK_BUFSIZE 64
#define USH_TOK_DELIM " \'';()"
//#define USH_LAST 0;

typedef struct s_parsing
{
    char *dst;
    t_list *arr;
    struct s_parsing *next;   
}              t_parsing;

t_parsing *mx_create_que(char *data, char operation);
void  parsing(char *str, t_parsing *strnew);
char * mystrtok(char * str, const char * delim, char **last);
void check_leaks();

#endif
